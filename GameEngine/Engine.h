#ifndef ENGINE_H
#define ENGINE_H

#include <unordered_set>
#include <vector>
#include <memory>
#include <algorithm>
#include <iostream>

#include "Component.h"
#include "System.h"
#include "Time.h"
#include "Graphics.h"
#include "Resources.h"

namespace af
{
	class Engine
	{
	private:
		static int currentid_;
		static std::unordered_set<int> entities_;
		static std::vector<std::shared_ptr<Component>> components_;
		static std::vector<std::unique_ptr<System>> systems_;
		static bool running_;
		static Time time_;
		static Graphics graphics_;
		static Resources resources_;

	public:
		template<typename T>
		static std::shared_ptr<T> GetComponent(int entity)
		{
			for (std::shared_ptr<Component> component : components_)
			{
				if (component->GetEntity() == entity)
				{
					std::shared_ptr<T> t = std::dynamic_pointer_cast<T>(component);

					if (t != nullptr)
					{
						return t;
					}
				}
			}

			return nullptr;
		}

		template<typename T>
		static std::vector<int> GetEntitiesWithComponent()
		{
			std::vector<int> entities;

			for (std::shared_ptr<Component> component : components_)
			{
				if (std::dynamic_pointer_cast<T>(component) != nullptr)
				{
					entities.push_back(component->GetEntity());
				}
			}

			return entities;
		}

		static void Stop();
		static int GenerateEntity();
		static void RemoveEntity(int entity);

		template<typename T>
		static std::shared_ptr<T> AddComponent(int entityadd)
		{
			for (int entity : entities_)
			{
				if (entity == entityadd)
				{
					if (std::find_if(components_.begin(), components_.end(), [entity](std::shared_ptr<Component> const component) { return component->GetEntity() == entity && std::dynamic_pointer_cast<T>(component) != nullptr; }) == components_.end())
					{
						std::shared_ptr<T> component = std::make_shared<T>();

						std::cout << "Added " << typeid(*component).name() << " to entity " << entity << "." << std::endl;
						component->entity_ = entity;
						components_.push_back(component);

						for (std::unique_ptr<System>& system : systems_)
						{
							system->AddComponent(component);
						}

						return component;
					}

					break;
				}
			}

			return nullptr;
		}

		template<typename T>
		static void RemoveComponent(int entity)
		{
			std::vector<std::shared_ptr<Component>>::iterator it = std::find_if(components_.begin(), components_.end(), [entity](std::shared_ptr<Component> const component) { return component->GetEntity() == entity && std::dynamic_pointer_cast<T>(component) != nullptr; });

			if (it != components_.end())
			{
				std::shared_ptr<Component> componentremove = (*it);

				std::cout << "Removed " << typeid(*componentremove).name() << " from entity " << componentremove->GetEntity() << "." << std::endl;
				for (std::unique_ptr<System>& system : systems_)
				{
					system->RemoveComponent(componentremove);
				}
				components_.erase(std::remove_if(components_.begin(), components_.end(), [componentremove](std::shared_ptr<Component> const component) { return component == componentremove && component->GetEntity() == componentremove->GetEntity(); }), components_.end());
			}
		}

		static void Initialize();
		static void Run();
		static void Dispose();
	};
}

#endif