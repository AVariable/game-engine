#include "Main.h"

#include "Engine.h"

Main::Main() 
{
}

Main::~Main() 
{
}

void Main::Run()
{
	af::Engine::Initialize();
	af::Engine::Run();
	af::Engine::Dispose();
}

int main(int argc, char* argv[])
{
	Main main;
	main.Run();

	return 0;
}