#ifndef SOUND_H
#define SOUND_H

#include <SDL_mixer.h>

#include "Resource.h"

namespace af
{
	class Sound : public Resource
	{
	private:
		Mix_Chunk* chunk_;

	public:
		Sound(Mix_Chunk* chunk);
		~Sound();
	};
}

#endif