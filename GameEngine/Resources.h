#ifndef RESOURCES_H
#define RESOURCES_H

#include <string>
#include <queue>
#include <map>
#include <SDL_thread.h>

namespace af
{
	class Resource;
	class Graphics;

	class Resources
	{
		friend class Engine;

	private:
		// Abstract class that aids in loading of resources that is extended to load various resource types
		class ResourceLoader
		{
		public:
			std::string path_;
			std::string name_;

		public:
			ResourceLoader(std::string path, std::string name);
			virtual ~ResourceLoader() = 0;

			// Abstract member function that will be called when resources are going to load
			virtual Resource* Load() = 0;
		};

		// Class that aids in loading of textures 
		class TextureLoader : public ResourceLoader
		{
		private:
			Graphics* graphics_;

		public:
			TextureLoader(Graphics* graphics, std::string path, std::string name);
			~TextureLoader();

			// Loads a texture and returns it as a resource
			Resource* Load();
		};

		// Class that aids in loading of sounds
		class SoundLoader : public ResourceLoader
		{
		public:
			SoundLoader(std::string path, std::string name);
			~SoundLoader();

			// Loads a sound and returns it as a resource
			Resource* Load();
		};

		// Class that aids in loading of music
		class MusicLoader : public ResourceLoader
		{
		public:
			MusicLoader(std::string path, std::string name);
			~MusicLoader();

			// Loads a music and returns it as a resource
			Resource* Load();
		};

		Graphics* graphics_;

		// Resources to be loaded
		std::queue<ResourceLoader*> queuedresources_;
		
		// Resources already loaded
		std::map<std::string, Resource*> resources_;

		// Thread that will load resources asynchronously
		SDL_Thread* loadingthread_;

		// Mutex used to access loading variable
		SDL_mutex* loadingmutex_;

		// Flag that tells if the resources have started to load
		bool loading_;

		// The total number of resources to be loaded
		int resourcestoload_;

	public:
		Resources();
		~Resources();

		// Returns a resource by it's name if found
		Resource* GetResource(std::string name);

		bool IsLoading();

		// Calculates the progress in percentage of the total number of resources to be loaded with the queued resources
		int GetProgress();

		// Loads all queued resources
		void StartLoading();

		// Finished loading all resources
		void FinishLoading();

		// Pushes a texture loader onto the queue of resources to be loaded
		void LoadTexture(std::string path, std::string name);

		// Pushes a sound loader onto the queue of resources to be loaded
		void LoadSound(std::string path, std::string name);

		// Pushes a music loader onto the queue of resources to be loaded
		void LoadMusic(std::string path, std::string name);

		// Destroy a resource by it's name if found
		void Destroy(std::string name);

		// Destroys all resources
		void Destroy();

	private:
		// Initialize the necessary variables to load resources
		bool Initialize(Graphics* graphics);

		// Disposes all loaded resources and deletes thread related variables
		void Dispose();

		// The thread that will be run when StartLoading is called
		static int ThreadLoad(void* data);
	};
}

#endif