#include "Sound.h"

af::Sound::Sound(Mix_Chunk* chunk)
{
	chunk_ = chunk;
}

af::Sound::~Sound()
{
	Mix_FreeChunk(chunk_);
	chunk_ = nullptr;
}