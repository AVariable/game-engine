#include "Texture.h"

af::Texture::Texture(SDL_Texture* texture)
{
	texture_ = texture;
	SDL_QueryTexture(texture, nullptr, nullptr, &width_, &height_);
}

af::Texture::~Texture()
{
	SDL_DestroyTexture(texture_);
	texture_ = nullptr;
}

int af::Texture::GetWidth()
{
	return width_;
}

int af::Texture::GetHeight()
{
	return height_;
}