#ifndef TIMER_H
#define TIMER_H

#include <SDL.h>

namespace af
{
	class Timer
	{
	private:
		// The clock time when the timer started
		Uint32 startTicks_;

		// The ticks stored when the timer was paused
		Uint32 pausedTicks_;

		// The timer status
		bool paused_;
		bool started_;

	public:
		Timer();
		~Timer();

		// The various clock actions
		void Start();
		void Stop();
		void Pause();
		void Unpause();

		// Gets the timer's time
		Uint32 GetTicks();

		// Checks the status of the timer
		bool IsStarted();
		bool IsPaused();
	};
}

#endif