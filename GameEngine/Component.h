#ifndef COMPONENT_H
#define COMPONENT_H

#include "Object.h"

namespace af
{
	class Component : public af::Object
	{
	protected:
		bool active_;
		int entity_;

	public:
		Component();
		virtual ~Component() = 0;
		int GetEntity() const;
		void SetEntity(int entity);
	};
}

#endif