#ifndef SYSTEM_H
#define SYSTEM_H

#include <memory>

namespace af
{
	class Component;

	class System
	{
	public:
		System();
		virtual ~System() = 0;
		virtual void AddComponent(std::shared_ptr<Component> component) = 0;
		virtual void RemoveComponent(std::shared_ptr<Component> component) = 0;
		virtual void RemoveComponentsWithEntity(int entity) = 0;
		virtual void Update() = 0;
	};
}

#endif