#ifndef S_SCRIPTS_H
#define S_SCRIPTS_H

#include <vector>

#include "../System.h"

namespace af
{
	class Script;

	class SScripts : public System
	{
	private:
		std::vector<std::shared_ptr<Script>> scripts_;

	public:
		SScripts();
		~SScripts();
		void AddComponent(std::shared_ptr<Component> component);
		void RemoveComponent(std::shared_ptr<Component> component);
		void RemoveComponentsWithEntity(int entity);
		void Update();
	};
}


#endif