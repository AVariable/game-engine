#include "SScripts.h"

#include <iostream>
#include <algorithm>
#include <typeinfo>

#include "../Components/Script.h"

af::SScripts::SScripts()
{
}

af::SScripts::~SScripts()
{
}

void af::SScripts::AddComponent(std::shared_ptr<Component> component)
{
	std::shared_ptr<Script> script = std::dynamic_pointer_cast<Script>(component);
	
	if (script != nullptr)
	{
		std::cout << "Adding " << typeid(*script).name() << " to Scripts System." << std::endl;
		scripts_.push_back(script);
	}
}

void af::SScripts::RemoveComponent(std::shared_ptr<Component> component)
{
	scripts_.erase(std::remove_if(scripts_.begin(), scripts_.end(), [component](std::shared_ptr<Script> const script) { return script == component; }), scripts_.end());
}

void af::SScripts::RemoveComponentsWithEntity(int entity)
{
	std::cout << "Removing scripts with entity " << entity << "." << std::endl;
	scripts_.erase(std::remove_if(scripts_.begin(), scripts_.end(), [entity](std::shared_ptr<Script> const script) { return script->GetEntity() == entity; }), scripts_.end());
}

void af::SScripts::Update()
{
	for (std::shared_ptr<Script> script : scripts_)
	{
		script->Update();
	}
}