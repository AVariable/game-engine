#include "Rectangle.h"

#include <SDL.h>

af::Rectangle::Rectangle(int x, int y, int width, int height)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}

af::Rectangle::~Rectangle()
{
}

bool af::Rectangle::Intersects(Rectangle& rectangle)
{
	SDL_Rect rect1 = { x, y, width, height };
	SDL_Rect rect2 = { rectangle.x, rectangle.y, rectangle.width, rectangle.height };
	SDL_bool result = SDL_HasIntersection(&rect1, &rect2);
	
	return result == 1 ? true : false;
}