#ifndef TIME_H
#define TIME_H

#include "Timer.h"

namespace af
{
	class Time
	{
		friend class Engine;

	private:
		Timer captimer_;
		Timer deltatimer_;

	public:
		Time();
		~Time();

		float GetDeltaTime();
	};
}

#endif