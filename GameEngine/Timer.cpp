#include "Timer.h"

af::Timer::Timer()
{
	startTicks_ = 0;
	pausedTicks_ = 0;
	paused_ = false;
	started_ = false;
}

af::Timer::~Timer()
{
}

void af::Timer::Start()
{
	// Start the timer
	started_ = true;

	// Unpause the timer
	paused_ = false;

	// Get the current clock time
	startTicks_ = SDL_GetTicks();
	pausedTicks_ = 0;
}

void af::Timer::Stop()
{
	// Stop the timer
	started_ = false;

	// Unpause the timer
	paused_ = false;

	// Clear tick variables
	startTicks_ = 0;
	pausedTicks_ = 0;
}

void af::Timer::Pause()
{
	// If the timer is running and isn't already paused
	if (started_ && !paused_)
	{
		// Pause the timer
		paused_ = true;

		// Calculate the paused ticks
		pausedTicks_ = SDL_GetTicks() - startTicks_;
		startTicks_ = 0;
	}
}

void af::Timer::Unpause()
{
	// If the timer is running and paused
	if (started_ && paused_)
	{
		// Unpause the timer
		paused_ = false;

		// Reset the starting ticks
		startTicks_ = SDL_GetTicks() - pausedTicks_;

		// Reset the paused ticks
		pausedTicks_ = 0;
	}
}

Uint32 af::Timer::GetTicks()
{
	// The actual timer time
	Uint32 time = 0;

	// If the timer is running
	if (started_)
	{
		// If the timer is paused
		if (paused_)
		{
			// Return the number of ticks when the timer was paused
			time = pausedTicks_;
		}
		else
		{
			// Return the current time minus the start time
			time = SDL_GetTicks() - startTicks_;
		}
	}

	return time;
}

bool af::Timer::IsStarted()
{
	return started_;
}

bool af::Timer::IsPaused()
{
	return paused_ && started_;
}