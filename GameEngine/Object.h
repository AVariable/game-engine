#ifndef OBJECT_H
#define OBJECT_H

namespace af
{
	class Object
	{
	private:
		static int currentid_;
		int id_;

	public:
		Object();
		virtual ~Object() = 0;

		bool operator ==(const Object& object) const;
	};
}

#endif