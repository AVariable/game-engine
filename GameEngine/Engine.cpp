#include "Engine.h"

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

int af::Engine::currentid_ = 0;
std::unordered_set<int> af::Engine::entities_;
std::vector<std::shared_ptr<af::Component>> af::Engine::components_;
std::vector<std::unique_ptr<af::System>> af::Engine::systems_;
bool af::Engine::running_ = false;
af::Time af::Engine::time_;
af::Graphics af::Engine::graphics_;
af::Resources af::Engine::resources_;

void af::Engine::Stop()
{
	running_ = false;
}

int af::Engine::GenerateEntity()
{
	currentid_++;
	entities_.insert(currentid_);

	std::cout << "Generated entity " << currentid_ << "." << std::endl;

	return currentid_;
}

void af::Engine::RemoveEntity(int entityremove)
{
	entities_.erase(entityremove);
	components_.erase(std::remove_if(components_.begin(), components_.end(), [entityremove](const std::shared_ptr<Component> component) { return component->GetEntity() == entityremove; }), components_.end());
	std::cout << "Removed entity " << entityremove << "." << std::endl;
}

void af::Engine::Initialize()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
		running_ = false;
	}

	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0"))
	{
		std::cout << "Warning: Nearest pixel texture filtering not enabled!" << std::endl;
	}

	int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
		running_ = false;
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		std::cout << "SDL_mixer could not initialize! SDL_mixer Error: " << Mix_GetError() << std::endl;
	}

	if (!graphics_.Initialize())
		running_ = false;

	if (!resources_.Initialize(&graphics_))
		running_ = false;

	running_ = true;
}

void af::Engine::Run()
{
	while (running_)
	{
		// Start the timer that will limit the framerate
		time_.captimer_.Start();

		SDL_Event e;

		// Handle SDL events
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				Stop();
			}
		}

		for (std::unique_ptr<System>& system : systems_)
		{
			system->Update();
		}

		// Start the delta timer
		time_.deltatimer_.Start();

		// Clear screen
		SDL_RenderClear(graphics_.renderer_);

		// Update screen
		SDL_RenderPresent(graphics_.renderer_);

		// If the current frame updated faster than the limit ticks per frame, delay to synchronize
		int frameticks = time_.captimer_.GetTicks();
		if (frameticks < graphics_.tickperframe_)
		{
			SDL_Delay(graphics_.tickperframe_ - frameticks);
		}
	}
}

void af::Engine::Dispose()
{
	std::cout << std::endl;

	for (int entity : entities_)
	{
		for (std::unique_ptr<System>& system : systems_)
		{
			system->RemoveComponentsWithEntity(entity);
		}
	}

	entities_.clear();
	components_.clear();
	systems_.clear();
	resources_.Dispose();
	graphics_.Dispose();

	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}