#include "Component.h"

af::Component::Component()
	:Object()
{
	active_ = true;
	entity_ = -1;
}

af::Component::~Component()
{
}

int af::Component::GetEntity() const
{
	return entity_;
}

void af::Component::SetEntity(int entity)
{
	entity_ = entity;
}