#include "Graphics.h"

#include <iostream>

af::Graphics::Graphics() {}
af::Graphics::~Graphics() {}

void af::Graphics::SetFps(int fps)
{
	fps_ = fps;
	tickperframe_ = 1000 / fps;
}

void af::Graphics::SetClearColor(Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_SetRenderDrawColor(renderer_, red, green, blue, 0xFF);
}

bool af::Graphics::Initialize()
{
	// Create a new window and save it in the graphics, if an error occurs return false
	window_ = SDL_CreateWindow("Application", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
	if (window_ == nullptr)
	{
		std::cout << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	// Create a new renderer with the saved window from graphics and save it in the graphics, if an error occurs return false
	renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);
	if (renderer_ == nullptr)
	{
		std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	SetFps(60);
	SetClearColor(0, 0, 0);

	return true;
}

void af::Graphics::Dispose()
{
	SDL_DestroyRenderer(renderer_);
	renderer_ = nullptr;
	SDL_DestroyWindow(window_);
	window_ = nullptr;
}