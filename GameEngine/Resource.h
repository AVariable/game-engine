#ifndef RESOURCE_H
#define RESOURCE_H

namespace af
{
	class Resource
	{
	public:
		Resource();
		virtual ~Resource() = 0;
	};
}

#endif