#ifndef TEXTURE_H
#define TEXTURE_H

#include <SDL.h>

#include "Resource.h"

namespace af
{
	class Texture : public Resource
	{
		friend class Renderer;
		
	private:
		SDL_Texture* texture_;
		int width_;
		int height_;

	public:
		Texture(SDL_Texture* texture);
		~Texture();

		int GetWidth();
		int GetHeight();
	};
}

#endif