#include "Object.h"

int af::Object::currentid_ = 0;

af::Object::Object()
{
	currentid_ += 1;
	id_ = currentid_;
}

af::Object::~Object() {}

bool af::Object::operator==(const Object& object) const
{
	return id_ == object.id_;
}