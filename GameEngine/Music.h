#ifndef MUSIC_H
#define MUSIC_H

#include <SDL_mixer.h>

#include "Resource.h"

namespace af
{
	class Music : public Resource
	{
	private:
		Mix_Music* music_;

	public:
		Music(Mix_Music* music);
		~Music();
	};
}

#endif