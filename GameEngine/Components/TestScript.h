#ifndef TEST_SCRIPT_H
#define TEST_SCRIPT_H

#include "Script.h"

namespace af
{
	class TestScript : public Script
	{
	public:
		TestScript();
		~TestScript();
		void Initialize();
		void Update();
	};
}

#endif