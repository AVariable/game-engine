#include "TestScript.h"

#include <iostream>

af::TestScript::TestScript()
	:Script()
{
}

af::TestScript::~TestScript()
{
}

void af::TestScript::Initialize()
{
	std::cout << "Initialized test script." << std::endl;
}

void af::TestScript::Update()
{
	std::cout << "Updated test script." << std::endl;
}