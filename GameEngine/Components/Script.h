#ifndef SCRIPT_H
#define SCRIPT_H

#include "../Component.h"

namespace af
{
	class Script : public Component
	{
		friend class SScripts;

	public:
		Script();
		virtual ~Script() = 0;

	protected:
		virtual void Initialize() = 0;
		virtual void Update() = 0;
	};
}


#endif