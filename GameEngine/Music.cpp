#include "Music.h"

af::Music::Music(Mix_Music* music)
{
	music_ = music;
}

af::Music::~Music()
{
	Mix_FreeMusic(music_);
	music_ = nullptr;
}