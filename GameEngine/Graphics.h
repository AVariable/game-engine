#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL.h>

namespace af
{
	class Graphics
	{
		friend class Engine;
		friend class Resources;

	private:
		SDL_Window* window_;
		SDL_Renderer* renderer_;
		int fps_;
		int tickperframe_;

	public:
		Graphics();
		~Graphics();

		void SetFps(int fps);
		void SetClearColor(Uint8 red, Uint8 green, Uint8 blue);

	private:
		bool Initialize();
		void Dispose();
	};
}

#endif