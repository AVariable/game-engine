#include "Resources.h"

#include <SDL_image.h>
#include <SDL_mixer.h>
#include <iostream>

#include "Graphics.h"
#include "Texture.h"
#include "Sound.h"
#include "Music.h"

af::Resources::Resources() {}

af::Resources::~Resources() {}

af::Resource* af::Resources::GetResource(std::string name)
{
	// Search for a resource with the same name
	auto iterator = resources_.find(name);

	// Check if the resource exists and return it
	if (iterator != resources_.end())
	{
		return iterator->second;
	}

	return nullptr;
}

bool af::Resources::IsLoading()
{
	return loading_;
}

int af::Resources::GetProgress()
{
	return static_cast<int>(roundf((static_cast<float>(resourcestoload_) - static_cast<float>(queuedresources_.size())) / static_cast<float>(resourcestoload_) * 100.f));
}

void af::Resources::StartLoading()
{
	SDL_LockMutex(loadingmutex_);
	if (!loading_)
	{
		resourcestoload_ = queuedresources_.size();
		loading_ = true;
		SDL_UnlockMutex(loadingmutex_);
		loadingthread_ = SDL_CreateThread(ThreadLoad, "ResourcesThread", this);
	}
	else
	{
		SDL_UnlockMutex(loadingmutex_);
	}
}

void af::Resources::FinishLoading()
{
	// Wait while the thread is loading resources, if the thread has stopped loading continue the normal process
	SDL_WaitThread(loadingthread_, nullptr);
}

void af::Resources::LoadTexture(std::string path, std::string name)
{
	// Add to the queue of resources to load a texture loader with the given information
	queuedresources_.push(new TextureLoader(graphics_, path, name));
}

void af::Resources::LoadSound(std::string path, std::string name)
{
	// Add to the queue of resources to load a sound loader with the given information
	queuedresources_.push(new SoundLoader(path, name));
}

void af::Resources::LoadMusic(std::string path, std::string name)
{
	// Add to the queue of resources to load a music loader with the given information
	queuedresources_.push(new MusicLoader(path, name));
}

void af::Resources::Destroy(std::string name)
{
	Resource* resource = GetResource(name);

	if (resource != nullptr)
	{
		delete resource;
		resources_.erase(name);
	}
}

void af::Resources::Destroy()
{
	// Destroy all resources
	for (std::pair<std::string, Resource*> pair : resources_)
	{
		delete pair.second;
		pair.second = nullptr;
	}
	resources_.clear();
}

bool af::Resources::Initialize(Graphics* graphics)
{
	graphics_ = graphics;
	loadingmutex_ = SDL_CreateMutex();

	if (loadingmutex_ == nullptr)
	{
		std::cout << "Could not create loading mutex of Resources." << std::endl;
		return false;
	}
	
	return true;
}

void af::Resources::Dispose()
{
	Destroy();
	SDL_DestroyMutex(loadingmutex_);
}

int af::Resources::ThreadLoad(void* data)
{
	Resources* resources = static_cast<Resources*>(data);

	// While the queue is not empty keep loading new resources
	while (!resources->queuedresources_.empty())
	{
		// Get the first resource loader of the queue
		ResourceLoader* resourceloader = resources->queuedresources_.front();

		// If the resource does not exist in the current map of resources, create it and store in the map
		if (resources->GetResource(resourceloader->name_) == nullptr)
		{
			resources->resources_.insert(std::make_pair(resourceloader->name_, resourceloader->Load()));
		}

		resources->queuedresources_.pop();

		delete resourceloader;
		resourceloader = nullptr;
	}

	SDL_LockMutex(resources->loadingmutex_);
	resources->loading_ = false;
	SDL_UnlockMutex(resources->loadingmutex_);

	return 0;
}

af::Resources::ResourceLoader::ResourceLoader(std::string path, std::string name)
{
	path_ = path;
	name_ = name;
}

af::Resources::ResourceLoader::~ResourceLoader() {}

af::Resources::TextureLoader::TextureLoader(Graphics* graphics, std::string path, std::string name)
	:ResourceLoader(path, name), graphics_(graphics) {}

af::Resources::TextureLoader::~TextureLoader() {}

af::Resource* af::Resources::TextureLoader::Load()
{
	// Load the image from the specified path
	SDL_Surface* loadedSurface = IMG_Load(path_.c_str());

	// Check if the image was loaded correctly
	if (loadedSurface != nullptr)
	{
		// Create a texture from the surface pixels of the loaded image
		SDL_Texture* sdlTexture = SDL_CreateTextureFromSurface(graphics_->renderer_, loadedSurface);

		// Get rid of the loaded surface
		SDL_FreeSurface(loadedSurface);
		loadedSurface = nullptr;

		// If the creation of the texture was unsuccessful, show a warning else return the created texture
		if (sdlTexture != nullptr)
		{
			return new Texture(sdlTexture);
		}
	}

	return nullptr;
}

af::Resources::SoundLoader::SoundLoader(std::string path, std::string name)
	:ResourceLoader(path, name) {}

af::Resources::SoundLoader::~SoundLoader() {}

af::Resource* af::Resources::SoundLoader::Load()
{
	// Create a chunk from a sound file
	Mix_Chunk* sound = Mix_LoadWAV(path_.c_str());

	// If the creation of the sound was unsuccessful, show a warning else insert it into the map
	if (sound != nullptr)
	{
		return new Sound(sound);
	}

	return nullptr;
}

af::Resources::MusicLoader::MusicLoader(std::string path, std::string name)
	:ResourceLoader(path, name) {}

af::Resources::MusicLoader::~MusicLoader() {}

af::Resource* af::Resources::MusicLoader::Load()
{
	// Create music from a music file
	Mix_Music* music = Mix_LoadMUS(path_.c_str());

	// If the creation of the music was unsuccessful, show a warning else insert it into the map
	if (music != nullptr)
	{
		return new Music(music);
	}

	return nullptr;
}